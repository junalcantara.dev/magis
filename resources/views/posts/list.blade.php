@extends('posts.post-layout')

@section('content')
<div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
        <table class="table-fixed w-full mt-5">
            <thead>
            <tr class="bg-gray-100">
                <th class="px-4 py-2 w-96 text-left">Title</th>
                <th class="px-4 py-2 text-left">Content</th>
                <th class="px-4 py-2 w-48">Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($posts as $post)
                <tr>
                    <td class="border px-4 py-2 text-sm">{{ $post->title }}</td>
                    <td class="border px-4 py-2 text-sm">{{ $post->content }}</td>
                    <td class="border px-4 py-2 text-center">
                        <a href="{{ route('posts.show', $post) }}" class="bg-blue-500 hover:bg-blue-700 text-white py-1 text-sm px-4 mr-2 rounded">View Post</a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>
@endsection
