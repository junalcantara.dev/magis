@extends('posts.post-layout')

@section('content')
<div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
        <h1 class="text-3xl mb-3">{{ $post->title }}</h1>

        <hr class="mb-3"/>

        <p class="mb-5">{{ $post->content }}</p>

        <a href="{{ route('posts.index') }}" class="bg-blue-500 hover:bg-blue-700 text-white py-1 text-sm px-4 mr-2 rounded">View All Post</a>
    </div>
</div>
@endsection
