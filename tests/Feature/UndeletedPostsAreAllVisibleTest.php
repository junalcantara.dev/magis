<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Post;

class UndeletedPostsAreAllVisibleTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function test_all_undeleted_post_are_visible()
    {
        $this->assertGuest();

        $posts = Post::factory(10)->create();

        $response = $this->get(route('posts.index'));

        $posts->each(function($post) use ($response){
            $response->assertSee($post->title);
        });
    }
}
