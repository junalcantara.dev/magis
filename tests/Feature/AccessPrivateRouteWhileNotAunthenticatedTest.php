<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Post;

class AccessPrivateRouteWhileNotAunthenticatedTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_guest_access_create_post()
    {
        $this->assertGuest();

        $response = $this->get(route('posts.create'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    public function test_guest_access_edit_post()
    {
        $this->assertGuest();

        $post = Post::factory()->create();

        $response = $this->get(route('posts.edit', $post));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    public function test_guest_access_store_post()
    {
        $this->assertGuest();

        $response = $this->post(route('posts.store'), [
            'title' => $this->faker->words(3, true),
            'content' => $this->faker->sentences(5, true)
        ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    public function test_guest_access_update_post()
    {
        $this->assertGuest();

        $post = Post::factory()->create();

        $response = $this->put(route('posts.update', $post));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
