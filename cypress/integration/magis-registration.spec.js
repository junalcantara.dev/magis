describe('example to-do app', () => {
    let name = "Jun Alcantara";
    let email = "junalcantara.dev@gmail.com";
    let password = "p@swOrd123";

    it('should visit the registration page', () => {
        cy.visit('/register')

        cy.get('[id=name]').type(`${name}`)
        cy.get('[id=email]').type(`${email}`)
        cy.get('[id=password]').type(`${password}`)
        cy.get('[id=password_confirmation]').type(`${password}{enter}`)

        cy.get('[data-cy=usermenudropdown]').click()
        cy.get('[data-cy=userlogout]').click()
    })

    it('Login the Registered User', () => {
        cy.visit('/login')

        cy.get('[id=email]').type(`${email}`)
        cy.get('[id=password]').type(`${password}{enter}`)

    });


})
